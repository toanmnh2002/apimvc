﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.IServices;
using Service.Model.Category;
using Service.Services;

namespace eStoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }/// <summary>
        /// Get All Categories
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>

        [HttpGet("AllCategory")]
        public async Task<ActionResult> GetCategories(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _categoryService.GetCategoriesAsync(pageIndex, pageSize);
            return Ok(response);
        }
        /// <summary>
        /// Add Category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("NewCategory")]
        public async Task<ActionResult> AddCategory(CreateCategoryModel model)
        {
            var response = await _categoryService.AddCategoryAsync(model);
            return Ok(response);
        }
        /// <summary>
        /// Update Category
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateCategory(int categoryId, UpdateCategoryModel model)
        {
            var response = await _categoryService.UpdateCategoryAsync(categoryId, model);
            return Ok(response);
        }
        /// <summary>
        /// Delete Category
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteCategory(int categoryId)
        {
            var response = await _categoryService.DeleteCategoryAsync(categoryId);
            return Ok(response);
        }
        /// <summary>
        /// Get Category By Category Id
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpGet("/{categoryId}")]
        public async Task<ActionResult> GetCategoryById(int categoryId)
        {
            var response = await _categoryService.GetCategoryAsync(categoryId);
            return Ok(response);
        }
        /// <summary>
        /// GetAllCategory
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public async Task<ActionResult> GetAllCategory()
        {
            var response = await _categoryService.GetCategoryAsync();
            return Ok(response);
        }
    }
}
