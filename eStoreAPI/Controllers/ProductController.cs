﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Service.IServices;
using Service.Model.Product;

namespace eStoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        /// <summary>
        /// Get Product
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet("Pagination")]
        public async Task<ActionResult> GetProduct(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _productService.GetProductsAsync(pageIndex, pageSize);
            return Ok(response);
        }
        /// <summary>
        /// Get Product By Id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet("ProductId/{productId}")]
        public async Task<ActionResult> GetProductById(int productId)
        {
            var response = await _productService.GetProductAsync(productId);
            return Ok(response);
        }
        /// <summary>
        /// Get Product By Name
        /// </summary>
        /// <param name="productName"></param>
        /// <returns></returns>
        [HttpGet("NameNavigation")]
        public async Task<ActionResult> GetProductByName([BindRequired, FromQuery] string productName)
        {
            var response = await _productService.GetProductAsync(productName);
            return Ok(response);
        }
        /// <summary>
        /// GetAllProduct
        /// </summary>
        /// <param name="productName"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> GetAllProduct()
        {
            var response = await _productService.GetAllProduct();
            return Ok(response);
        }
        /// <summary>
        /// Get Product By CategoryId
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet("Category/{categoryId}")]
        public async Task<ActionResult> GetProductByCategoryId(int categoryId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _productService.GetProductsAsync(categoryId, pageIndex, pageSize);
            return Ok(response);
        }
        /// <summary>
        /// Add Product
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("NewProduct")]
        public async Task<ActionResult> AddProduct(CreateProductModel model)
        {
            var response = await _productService.AddProductAsync(model);
            return Ok(response);
        }
        /// <summary>
        /// Update Product
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateProduct(int productId, UpdateProductModel model)
        {
            var response = await _productService.UpdateProductAsync(productId, model);
            return Ok(response);
        }
        /// <summary>
        /// Delete Update
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteUpdate(int productId)
        {
            var response = await _productService.DeleteProductAsync(productId);
            return Ok(response);
        }
    }
}
