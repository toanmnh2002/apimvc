﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repository.Interfaces;
using Repository.IServices;
using Service.IServices;
using Service.Model;
using Service.Model.Order;
using Service.Services;
using System.Runtime.InteropServices;

namespace eStoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IValidator<CreateOrderModel> _validation;


        public OrderController(IOrderService orderService, IValidator<CreateOrderModel> validation)
        {
            _orderService = orderService;
            _validation = validation;
        }


        /// <summary>
        /// Get Order By MemberId
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet("memberId/{memberId}")]
        public async Task<ActionResult> GetOrderByMemberId(int memberId, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.GetOrdersAsync(memberId, pageIndex, pageSize);
            return Ok(response);
        }
        /// <summary>
        /// GetOrders
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet("AllOrder")]
        public async Task<ActionResult> GetOrders(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.GetOrderAsync(pageIndex, pageSize);
            return Ok(response);
        }
        /// <summary>
        /// SearchOrderByDateRange
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> SearchOrderByDateRange(DateTime startDate, DateTime endDate, int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.SearchOrderAsync(startDate, endDate, pageIndex, pageSize);
            return Ok(response);
        }
        /// <summary>
        /// GetOrderById
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpGet("orderId/{orderId}")]
        public async Task<ActionResult> GetOrderById(int orderId)
        {
            var response = await _orderService.GetOrderAsync(orderId);
            return Ok(response);
        }
        /// <summary>
        /// AddOrder
        /// </summary>
        /// <param name="createOrderModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddOrder(CreateOrderModel createOrderModel)
        {
            #region
            var result = await _validation.ValidateAsync(createOrderModel);
            if (!result.IsValid)
            {
                return BadRequest(result.Errors.Select(
                   x => new ErrorValidationModel
                   {
                       FieldName = x.PropertyName,
                       ErrorMessage = x.ErrorMessage
                   }
                    ));
            }
            #endregion
            var response = await _orderService.AddOrderAsync(createOrderModel);
            return Ok(response);
        }
        /// <summary>
        /// GetOrderDetailsByOrder
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet("OrderDetails")]
        public async Task<ActionResult> GetOrderDetailByEachOrderAsync(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _orderService.GetOrderDetailByEachOrderAsync(pageIndex, pageSize);
            return Ok(response);
        }

        /// <summary>
        /// UpdateOrder
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="updateOrderModel"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateOrder(int orderId, UpdateOrderModel updateOrderModel)
        {
            var response = await _orderService.UpdateOrderAsync(orderId, updateOrderModel);
            return Ok(response);
        }
        /// <summary>
        /// DeleteOrder
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteOrder(int orderId)
        {
            var response = await _orderService.DeleteOrderAsync(orderId);
            return Ok(response);
        }
        /// <summary>
        /// SaleFromDatetoDate
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet("Statistic")]
        public async Task<ActionResult> GetSaleReport([FromQuery] DateTime startDate, [FromQuery] DateTime endDate)
        {
            var response = await _orderService.GetSalesReport(startDate, endDate);
            return Ok(response);
        }
        /// <summary>
        /// SumOfSaleAllTime
        /// </summary>
        /// <returns></returns>
        [HttpPost("TestStatistic")]
        public async Task<ActionResult> CalculateSumOrderAllTime()
        {
            var response = await _orderService.CalculateSumOrderAllTime();
            return Ok(response);
        }
        /// <summary>
        /// Get Pie Chart
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("GetPieChart")]
        public async Task<ActionResult> GetPieChart(int year)
        {
            var response = await _orderService.GetRevenueStructure(year);
            return Ok(response);
        }
        /// <summary>
        /// Get Bar Chart
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet("GetBarChart")]
        public async Task<ActionResult> GetBarChart(DateTime startDate, DateTime endDate)
        {
            var response = await _orderService.GetProductWithAmount(startDate, endDate);
            return Ok(response);
        }
    }
}
