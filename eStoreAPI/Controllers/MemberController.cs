﻿using BusinessObject;
using Repository.IServices;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using BusinessObject.Entities;
using Microsoft.AspNetCore.Authorization;
using Service.Model.MemberModel;

namespace eStoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemberController : ControllerBase
    {
        private readonly IMemberService _memberService;

        public MemberController(IMemberService memberService)
        {
            _memberService = memberService;
        }
        /// <summary>
        /// Login
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpPost]
        public async Task<ActionResult> Login(LoginModel login)
        {
            try
            {
                if (string.IsNullOrEmpty(login.Email) ||
                    string.IsNullOrEmpty(login.Password))
                {
                    throw new Exception("Login Information is invalid!! Please check again...");
                }
                Member loginMember = await _memberService.LoginAsync(login);
                if (loginMember is null)
                {
                    throw new Exception("Failed to login! Please check the information again...");
                }
                MemberRole memberRole = MemberRole.USER;
                Member defaultMember = _memberService.GetAdmin();
                if (loginMember.Email.Equals(defaultMember.Email))
                {
                    memberRole = MemberRole.ADMIN;
                }
                var claims = new List<Claim>
                {
                    new Claim("MemberId", loginMember.MemberId.ToString()),
                    new Claim(ClaimTypes.Email, loginMember.Email),
                    new Claim(ClaimTypes.NameIdentifier, loginMember.MemberId.ToString()),
                    new Claim(ClaimTypes.Role, memberRole.ToString())
                };
                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var authProperties = new AuthenticationProperties
                {
                    AllowRefresh = false,
                    IsPersistent = true
                };

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity), authProperties);

                //loginMember.MemberId = 0;
                loginMember.Password = "";
                loginMember.Orders = null;

                MemberWithRole returnMember = new MemberWithRole(loginMember);
                returnMember.MemberRoleString = memberRole.ToString();

                return StatusCode(200, returnMember);
            }
            catch
            {
                throw new Exception("Fail to login!");
            }
        }
        /// <summary>
        /// Logout
        /// </summary>
        /// <returns></returns>
        [HttpPost("exit")]
        public async Task<ActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return StatusCode(204);
        }
        /// <summary>
        /// Get Member
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> GetMembers(int pageIndex = 0, int pageSize = 10)
        {
            var response = await _memberService.GetMembersAsync(pageIndex, pageSize);
            return Ok(response);
        }
        /// <summary>
        /// Get all Member
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet("AllMember")]
        public async Task<ActionResult> GetMembers()
        {
            var response = await _memberService.GetMembersAsync();
            return Ok(response);
        }
        /// <summary>
        /// Add Member
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("Member")]
        public async Task<ActionResult> AddMember(CreateMemberModel model)
        {
            var response = await _memberService.AddMemberAsync(model);
            return Ok(response);
        }
        /// <summary>
        /// Update Member
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateMember(int memberId, UpdateMemberModel model)
        {
            var response = await _memberService.UpdateMemberAsync(memberId, model);
            return Ok(response);
        }
        /// <summary>
        /// Delete Member
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteMember(int memberId)
        {
            var response = await _memberService.DeleteMember(memberId);
            return Ok(response);
        }
        /// <summary>
        /// Get Member By MemberId
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        [HttpGet("{memberId}")]
        public async Task<ActionResult> GetMemberById(int memberId)
        {
            var response = await _memberService.GetMemberByIdAsync(memberId);
            return Ok(response);
        }
        /// <summary>
        /// Get Member By Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet("/User/{email}")]
        public async Task<ActionResult> GetMemberByEmail(string email)
        {
            var response = await _memberService.GetMemberByEmailAsync(email);
            return Ok(response);
        }
    }
}
