﻿using Repository;
using Repository.IServices;
using Microsoft.OpenApi.Models;
using System.Reflection;
using Service.IServices;
using Service.Services;
using FluentValidation;
using Service.Model.Order;
using Service.FluentValidation;

namespace eStoreAPI
{
    public static class DependencyInjection
    {
        public static IServiceCollection APIConfiguration(this IServiceCollection services)
        {
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            services.AddControllersWithViews()
    .AddNewtonsoftJson(options =>
    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
);
            services.AddScoped<IClaimsServices, ClaimsServices>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IMemberService, MemberService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IOrderService, OrderService>();

            services.AddScoped<IValidator<CreateOrderModel>,CreateOrderValidation>();
            services.AddSwaggerGen(
                    c =>
                    {
                        c.SwaggerDoc("v1", new OpenApiInfo
                        {
                            Title = "FStore",
                            Version = "v1",
                            Description = "This is the API of Assignment written by Toan Nguyen",
                            Contact = new OpenApiContact
                            {
                                Url = new Uri("https://google.com")
                            }
                        });
                        var xmlFileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                        c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFileName));
                    });
            return services;
        }
    }
}
