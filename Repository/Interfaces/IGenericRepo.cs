﻿using BusinessObject.Enum;
using Repository.Commons;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace Repository.Interfaces
{
    public interface IGenericRepo<T> where T : class
    {
        Task CreateAsync(T entity);
        Task CreateRangeAsync(IEnumerable<T> entities);
        void UpdateAsync(T entity);
        void UpdateRangeAsync(IEnumerable<T> entities);
        void DeleteAsync(T entity);
        Task<IEnumerable<T>> GetAllAsync();
        Task<IEnumerable<T>> GetAllEntitiesGenericAsync(Expression<Func<T, bool>>? expression = null, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);

        Task<T> GetEntityCondition(Expression<Func<T, bool>> expression, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null);
        Task<Pagination<T>> ToPagination(int pageIndex = 0, int pageSize = 10);
        Task<Pagination<T>> FilterEntity(Expression<Func<T, bool>> expression = null,
        Func<IQueryable<T>, IQueryable<T>> include = null,
        int pageIndex = 0,
        int pageSize = 10,
        Expression<Func<T, object>> sortColumn = null,
        SortDirection sortDirection = SortDirection.Descending);
    }
}
