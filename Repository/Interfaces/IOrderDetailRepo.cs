﻿using BusinessObject.Entities;
using Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IOrderDetailRepo : IGenericRepo<OrderDetail>
    {
        Task<List<Table>> GetRevenueStructure(int year);
        Task<List<Table>> GetProductWithAmount(DateTime startDate, DateTime endDate);
    }
}
