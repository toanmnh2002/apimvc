﻿using BusinessObject.Entities;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class OrderDetailRepo : GenericRepo<OrderDetail>, IOrderDetailRepo
    {
        private readonly FstoreDbContext _context;

        public OrderDetailRepo(FstoreDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<List<Table>> GetRevenueStructure(int year) => await _context.OrderDetails
                                      .Where(o => o.Order.OrderDate.Year == year)
                                      .GroupBy(d => new { d.Product.Category.CategoryId, d.Product.Category.CategoryName })
                                      .Select(t => new Table
                                      {
                                          Key = t.Key.CategoryName,
                                          Value = (int)t.Sum(k => k.Quantity * k.UnitPrice)
                                      }).ToListAsync();

        public async Task<List<Table>> GetProductWithAmount(DateTime startDate, DateTime endDate) => await _context.OrderDetails
                                  .Where(x => x.Order.OrderDate >= startDate && x.Order.OrderDate < endDate)
                                  .GroupBy(x => new { x.Product.ProductId, x.Product.ProductName })
                                  .Select(t => new Table
                                  {
                                      Key = t.Key.ProductName,
                                      Value = t.Sum(k => k.Quantity)
                                  })
            .OrderByDescending(x => x.Value)
                                  .ToListAsync();

    }
    public class Table
    {
        public string Key { get; set; }
        public int Value { get; set; }
    }
}
