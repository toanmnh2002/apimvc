﻿using BusinessObject.Entities;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class CategoryRepo : GenericRepo<Category>, ICategoryRepo
    {
        private readonly FstoreDbContext _context;
        public CategoryRepo(FstoreDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<int> GetNextId()
        {
            int nextId = -1;
            try
            {
                nextId = await _context.Categories.MaxAsync(x => x.CategoryId) + 1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return nextId;
        }
    }
}
