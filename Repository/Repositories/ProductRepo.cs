﻿using BusinessObject.Entities;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class ProductRepo : GenericRepo<Product>, IProductRepo
    {
        private readonly FstoreDbContext _context;
        public ProductRepo(FstoreDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<int> GetNextId()
        {
            int nextId = -1;
            try
            {
                nextId = await _context.Products.MaxAsync(x => x.ProductId) + 1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return nextId;
        }
    }
}
