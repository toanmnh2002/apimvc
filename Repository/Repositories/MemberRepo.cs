﻿using BusinessObject.Entities;
using Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class MemberRepo : GenericRepo<Member>, IMemberRepo
    {
        private readonly FstoreDbContext _context;
        public MemberRepo(FstoreDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<int> GetNextId()
        {
            int nextId = -1;
            try
            {
                nextId = await _context.Members.MaxAsync(x => x.MemberId) + 1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return nextId;
        }
    }
}
