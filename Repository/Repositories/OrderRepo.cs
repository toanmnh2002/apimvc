﻿using BusinessObject.Entities;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class OrderRepo:GenericRepo<Order>,IOrderRepo
    {
        private readonly FstoreDbContext _context;

        public OrderRepo(FstoreDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<int> GetNextId()
        {
            int nextId = -1;
            try
            {
                nextId = await _context.Orders.MaxAsync(x => x.OrderId) + 1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return nextId;
        }
    }
}
