﻿using BusinessObject.Entities;
using BusinessObject.Enum;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Repository.Interfaces;
using System.Linq.Expressions;
using Repository.Commons;

namespace Repository.Repositories
{
    public class GenericRepo<T> : IGenericRepo<T> where T : class
    {
        protected readonly DbSet<T> _dbSet;
        public GenericRepo(FstoreDbContext context)
        {
            _dbSet = context.Set<T>();
        }

        public async Task CreateAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async Task CreateRangeAsync(IEnumerable<T> entities)
        {
            await _dbSet.AddRangeAsync(entities);
        }
        public void UpdateAsync(T entity)
        {
            _dbSet.Update(entity);
        }

        public void UpdateRangeAsync(IEnumerable<T> entities)
        {
            _dbSet.RemoveRange(entities);
        }
        public void DeleteAsync(T entity)
        {
            _dbSet.Remove(entity);
        }
        public async Task<Pagination<T>> FilterEntity(
            Expression<Func<T, bool>> expression = null,
            Func<IQueryable<T>, IQueryable<T>> include = null,
            int pageIndex = 0, int pageSize = 10,
            Expression<Func<T, object>> sortColumn = null,
            SortDirection sortDirection = SortDirection.Descending)
        {
            var query = _dbSet.AsQueryable();

            // Include related entities if specified
            if (include is not null)
            {
                query = include(query);
            }

            // Apply filtering if specified
            if (expression is not null)
            {
                query = query.Where(expression);
            }

            // Apply sorting if specified
            if (sortColumn is not null)
            {
                query = sortDirection == SortDirection.Ascending
                    ? query.OrderBy(sortColumn)
                    : query.OrderByDescending(sortColumn);
            }

            // Calculate the total item count for the query
            var itemCount = await query.CountAsync();

            // Apply pagination to the query
            var items = await query.Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .ToListAsync();

            // Create the pagination result
            var result = new Pagination<T>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items,
            };

            return result;
        }
        public async Task<IEnumerable<T>> GetAllEntitiesGenericAsync(Expression<Func<T, bool>>? expression = null, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            //IQueryable<T> query = _appDBContext.Set<T>();
            IQueryable<T> query = _dbSet;
            if (include is not null)
            {
                query = include(query);
            }
            if (expression is not null)
            {
                query = query.Where(expression);
            }
            return await query.ToListAsync();
        }
        public async Task<IEnumerable<T>> GetAllAsync() => await _dbSet.AsNoTracking().ToListAsync();
        public async Task<T> GetEntityCondition(Expression<Func<T, bool>> expression, Func<IQueryable<T>, IIncludableQueryable<T, object>>? include = null)
        {
            IQueryable<T> query = _dbSet;
            if (include is not null)
            {
                query = include(query);
            }
            return await query.FirstOrDefaultAsync(expression);
        }

        public async Task<Pagination<T>> ToPagination(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<T>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemCount = itemCount,
                Items = items,
            };
            return result;
        }
    }
}
