﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BusinessObject.Entities;
using eStoreClient.Models;
using StoreClient.Utils;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Service.Model.SaleReportModel;
using Microsoft.AspNetCore.Authorization;
using System.Data;

namespace eStoreClient.Controllers
{
    public class ProductController : Controller
    {
        private readonly FstoreDbContext _context;
        private readonly ApiUrl _apiUrl;
        public ProductController(FstoreDbContext context, ApiUrl apiUrl)
        {
            _context = context;
            _apiUrl = apiUrl;
        }

        // GET: Product
        public async Task<IActionResult> Index([FromQuery] string searchString)
        {
            try
            {//Product/NameNavigation?productName=candy
                HttpResponseMessage response;
                string fetchUrl = _apiUrl.Default + "/Product";
                if (!string.IsNullOrEmpty(searchString))
                {
                    ViewData["search"] = searchString;
                    fetchUrl += "/NameNavigation?productName=" + searchString;

                }
                response = await eStoreClientUtils.ApiRequest(
                    eStoreHttpMethod.GET,
                    fetchUrl);

                if (response.IsSuccessStatusCode)
                {
                    IEnumerable<Product> products =
                        await response.Content.ReadAsAsync<IEnumerable<Product>>();
                    return View(products);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("AccessDenied", "Login");
                }
                else
                {
                    throw new Exception(await response.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                ViewData["Products"] = ex.Message;
                return View();
            }
        }
        private async Task<IEnumerable<Category>> GetCategoriesAsync()
        {
            HttpResponseMessage response = await eStoreClientUtils.ApiRequest(
                eStoreHttpMethod.GET,
                _apiUrl.Default + "/Category");

            if (response.IsSuccessStatusCode)
            {
                IEnumerable<Category> categories =
                    await response.Content.ReadAsAsync<IEnumerable<Category>>();
                return categories;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new Exception("Failed to get categories! Please log out and log in again...");
            }
            else
            {
                throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }
        // GET: Product/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .Include(p => p.Category)
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Product/Create
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Create()
        {
            try
            {
                IEnumerable<Category> categories =
                    await GetCategoriesAsync();
                ViewData["CategoryId"] = new SelectList(categories, "CategoryId", "CategoryName");
                return View();
            }
            catch (Exception ex)
            {
                ViewData["Products"] = ex.Message;
                return View();
            }
        }

        // POST: Product/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductId,CategoryId,ProductName,Weight,UnitPrice,UnitsInStock")] Product product)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    IEnumerable<Category> categories =
                        await GetCategoriesAsync();
                    ViewData["CategoryId"] = new SelectList(categories, "CategoryId", "CategoryName");

                    HttpResponseMessage response = await eStoreClientUtils.ApiRequest(
                    eStoreHttpMethod.POST,
                    _apiUrl.Default + "/Product/NewProduct", product);

                    if (response.IsSuccessStatusCode)
                    {
                        Product createdProduct = await response.Content.ReadAsAsync<Product>();
                        if (createdProduct == null)
                        {
                            throw new Exception("Failed to create product!! Please check again...");
                        }
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        return RedirectToAction("AccessDenied", "Login");
                    }
                    else
                    {
                        throw new Exception(await response.Content.ReadAsStringAsync());
                    }
                }
                catch (Exception ex)
                {
                    ViewData["Products"] = ex.Message;
                    return View(product);
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Product/Edit/5
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(_context.Categories, "CategoryId", "CategoryName", product.CategoryId);
            return View(product);
        }

        // POST: Product/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ProductId,CategoryId,ProductName,Weight,UnitPrice,UnitsInStock")] Product product)
        {
            if (id != product.ProductId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.ProductId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.Categories, "CategoryId", "CategoryName", product.CategoryId);
            return View(product);
        }

        // GET: Product/Delete/5
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .Include(p => p.Category)
                .FirstOrDefaultAsync(m => m.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Product/Delete/5
        [Authorize(Roles = "ADMIN")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Products == null)
            {
                return Problem("Entity set 'FstoreDbContext.Products'  is null.");
            }
            var product = await _context.Products.FindAsync(id);
            if (product != null)
            {
                _context.Products.Remove(product);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return (_context.Products?.Any(e => e.ProductId == id)).GetValueOrDefault();
        }
    }
}
