﻿using eStoreClient.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StoreClient.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eStoreClient.Controllers
{
    public class LogoutController : Controller
    {
        private readonly ApiUrl _api;

        public LogoutController(ApiUrl api)
        {
            _api = api;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            await eStoreClientUtils.ApiRequest(
                eStoreHttpMethod.POST,
                _api.Default + "/Member/logout");
            await HttpContext.SignOutAsync();
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Login");
        }
    }
}