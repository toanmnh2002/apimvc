﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BusinessObject.Entities;
using StoreClient.Utils;
using BusinessObject;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using eStoreClient.Models;

namespace eStoreClient.Controllers
{
    public class LoginController : Controller
    {
        private readonly FstoreDbContext _context;
        private readonly ApiUrl _url;
        public LoginController(FstoreDbContext context, ApiUrl url)
        {
            _context = context;
            _url = url;
        }

        // GET: Login
        [AllowAnonymous]
        public async Task<IActionResult> Index([FromQuery] string returnUrl )
        {
            if (User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                if (eStoreClientUtils.IsAdmin(User))
                {
                    return RedirectToAction("Index", "Member");
                }
                else
                {
                    return RedirectToAction("Index", "Order");
                }
            }
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index([FromForm, Bind("Email", "Password")] MemberLoginModel memberLoginInfo,
                                                [FromForm, Bind("ReturnUrl")] string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                if (eStoreClientUtils.IsAdmin(User))
                {
                    return RedirectToAction("Index", "Members");
                }
                else
                {
                    return RedirectToAction("Index", "Orders");
                }
            }
            try
            {
                HttpResponseMessage response = await eStoreClientUtils.ApiRequest(
                eStoreHttpMethod.POST,
                _url.Default + "/Member",
                memberLoginInfo);

                if (response.IsSuccessStatusCode)
                {
                    MemberWithRole loginMember = await response.Content.ReadAsAsync<MemberWithRole>();
                    if (loginMember == null)
                    {
                        throw new Exception("Failed to login! Please check again...");
                    }
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Email, loginMember.Email),
                        new Claim(ClaimTypes.NameIdentifier, loginMember.MemberId.ToString()),
                        new Claim(ClaimTypes.Role, loginMember.MemberRoleString)
                    };

                    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    var memberPrincipal = new ClaimsPrincipal(new[] { claimsIdentity });

                    await HttpContext.SignInAsync(memberPrincipal);

                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        if (eStoreClientUtils.IsAdmin(User))
                        {
                            return RedirectToAction("Index", "Members");
                        }
                        else
                        {
                            return RedirectToAction("Index", "Order");
                        }
                    }
                    return Redirect(returnUrl);

                }
                else
                {
                    throw new Exception(await response.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                ViewData["Login"] = ex.Message;
                return View();
            }
        }

        [AllowAnonymous]
        public IActionResult AccessDenied([FromQuery] string returnUrl)
        {
            ViewData["returnUrl"] = returnUrl;
            return View();
        }
    }
}
