﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BusinessObject.Entities;
using StoreClient.Utils;
using System.Security.Claims;
using eStoreClient.Models;
using Repository.Commons;
using Service.Model.Order;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Service.Model.SaleReportModel;
using System.IO.Pipelines;
using Azure;
using Microsoft.Extensions.Options;
using Repository.Repositories;
using Newtonsoft.Json;

namespace eStoreClient.Controllers
{
    public class OrderController : Controller
    {
        private readonly FstoreDbContext _context;
        private readonly ApiUrl _url;
        public OrderController(FstoreDbContext context, ApiUrl url)
        {
            _context = context;
            _url = url;
        }

        // GET: Order
        [Authorize]
        public async Task<IActionResult> Index()
        {
            try
            {
                HttpResponseMessage response;
                string fetchUrl = _url.Default + $"/Order/AllOrder?";

                if (!eStoreClientUtils.IsAdmin(User))
                {
                    string memberId = User.Claims.FirstOrDefault(claim => claim.Type.Equals(ClaimTypes.NameIdentifier)).Value;
                    fetchUrl += "/Order/memberId" + memberId;
                }
                response = await eStoreClientUtils.ApiRequest(
                    eStoreHttpMethod.GET,
                    fetchUrl);
                if (response.IsSuccessStatusCode)
                {
                    Pagination<OrderViewModel> orders =
                        await response.Content.ReadAsAsync<Pagination<OrderViewModel>>();
                    return View(orders);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("AccessDenied", "Login");
                }
                else
                {
                    throw new Exception(await response.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                ViewData["Orders"] = ex.Message;
                return View();
            }
        }

        // GET: Order/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Orders == null)
            {
                return NotFound();
            }
            HttpResponseMessage response = await eStoreClientUtils.ApiRequest(
                    eStoreHttpMethod.GET,
                    _url.Default + "/Order/orderId/" + id);
            if (response.IsSuccessStatusCode)
            {
                Order order = await response.Content.ReadAsAsync<Order>();
                return View(order);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                return RedirectToAction("AccessDenied", "Login");
            }
            else
            {
                throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }

        private async Task<IEnumerable<Member>> GetMembersAsync()
        {
            HttpResponseMessage response = await eStoreClientUtils.ApiRequest(
                eStoreHttpMethod.GET,
                _url.Default + "/Member/AllMember");

            if (response.IsSuccessStatusCode)
            {
                IEnumerable<Member> members =
                    await response.Content.ReadAsAsync<IEnumerable<Member>>();
                return members;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new Exception("Failed to get members! Please log out and log in again...");
            }
            else
            {
                throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }
        private async Task<IEnumerable<Product>> GetProductsAsync()
        {
            HttpResponseMessage response = await eStoreClientUtils.ApiRequest(
                eStoreHttpMethod.GET,
                _url.Default + "/Product");

            if (response.IsSuccessStatusCode)
            {
                IEnumerable<Product> products =
                    await response.Content.ReadAsAsync<IEnumerable<Product>>();
                return products;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new Exception("Failed to get products! Please log out and log in again...");
            }
            else
            {
                throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }

        // GET: Order/Create
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Create()
        {
            try
            {
                ViewData["ProductsSelectedList"] = new SelectList(await GetProductsAsync(), "ProductId", "ProductName");
                IEnumerable<Member> members =
                    await GetMembersAsync();
                ViewData["MemberId"] = new SelectList(members, "MemberId", "Email");
                ViewData["OrderDetails"] = HttpContext.Session.GetData<List<OrderDetail>>("OrderCart");
                return View();
            }
            catch (Exception ex)
            {
                ViewData["Orders"] = ex.Message;
                return View();
            }
        }

        // POST: Order/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrderId,MemberId,OrderDate,RequiredDate,ShippedDate,Freight")] Order order)
        {
            try
            {

                IEnumerable<Member> members =
                    await GetMembersAsync();
                ViewData["MemberId"] = new SelectList(members, "MemberId", "Email", order.MemberId);
                ViewData["ProductsSelectedList"] = new SelectList(await GetProductsAsync(), "ProductId", "ProductName");
                ViewData["OrderDetails"] = HttpContext.Session.GetData<List<OrderDetail>>("OrderCart");

                IEnumerable<OrderDetail> orderDetails = HttpContext.Session.GetData<List<OrderDetail>>("OrderCart");
                if (orderDetails == null || !orderDetails.Any())
                {
                    throw new Exception("Please add at least one item to your order details to create order");
                }
                order.OrderDetails = orderDetails.ToList();
                foreach (var od in order.OrderDetails)
                {
                    od.Product = null;
                }
                HttpResponseMessage response = await eStoreClientUtils.ApiRequest(
                eStoreHttpMethod.POST,
                _url.Default + "/Order", order);

                if (response.IsSuccessStatusCode)
                {
                    Order createdOrder = await response.Content.ReadAsAsync<Order>();
                    if (createdOrder == null)
                    {
                        throw new Exception("Failed to create order!! Please check again...");
                    }
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("AccessDenied", "Login");
                }
                else
                {
                    throw new Exception("Validation error! Please check your input!");
                }
            }
            catch (Exception ex)
            {
                ViewData["Orders"] = ex.Message;
                return View();
            }
            HttpContext.Session.SetData("OrderCart", null);
            return RedirectToAction(nameof(Index));

        }
        private async Task<Product> GetProduct(int productId)
        {
            HttpResponseMessage response = await eStoreClientUtils.ApiRequest(
                    eStoreHttpMethod.GET,
                    _url.Default + "/Product/ProductId/" + productId);

            if (response.IsSuccessStatusCode)
            {
                Product product = await response.Content.ReadAsAsync<Product>();
                return product;
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new Exception("Failed to get products! Please log out and log in again...");
            }
            else
            {
                throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }
        private bool IsExistedInCart(IEnumerable<OrderDetail> cart, int productId)
        {
            return cart.Any(item => item.ProductId.Equals(productId));
        }
        [HttpPost]
        public async Task<IActionResult> AddToCart([Bind("ProductId", "Quantity", "Discount")] OrderDetailsModel orderDetails)
        {
            try
            {

                Product product = await GetProduct(orderDetails.ProductId);
                if (orderDetails.Quantity > product.UnitsInStock)
                {
                    throw new Exception("Quantity must be less than " + product.UnitsInStock);
                }
                List<OrderDetail> cart = HttpContext.Session.GetData<List<OrderDetail>>("OrderCart");
                if (cart == null)
                {
                    cart = new List<OrderDetail>();
                    cart.Add(new OrderDetail
                    {
                        ProductId = orderDetails.ProductId,
                        Product = product,
                        UnitPrice = product.UnitPrice,
                        Quantity = orderDetails.Quantity,
                        Discount = orderDetails.Discount
                    });
                }
                else
                {
                    if (IsExistedInCart(cart, orderDetails.ProductId))
                    {
                        int index = cart.FindIndex(od => od.ProductId.Equals(orderDetails.ProductId));
                        cart[index].Quantity += orderDetails.Quantity;
                    }
                    else
                    {
                        cart.Add(new OrderDetail
                        {
                            ProductId = orderDetails.ProductId,
                            Product = product,
                            UnitPrice = product.UnitPrice,
                            Quantity = orderDetails.Quantity,
                            Discount = orderDetails.Discount
                        });
                    }
                }
                HttpContext.Session.SetData("OrderCart", cart);

                return RedirectToAction(nameof(Create));
            }
            catch (Exception ex)
            {
                TempData["Orders"] = ex.Message;
                return RedirectToAction(nameof(Create));
            }

        }

        // GET: Order/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Orders == null)
            {
                return NotFound();
            }

            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            ViewData["MemberId"] = new SelectList(_context.Members, "MemberId", "City", order.MemberId);
            return View(order);
        }

        // POST: Order/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("OrderId,MemberId,OrderDate,RequiredDate,ShippedDate,Freight")] Order order)
        {
            if (id != order.OrderId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.OrderId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MemberId"] = new SelectList(_context.Members, "MemberId", "City", order.MemberId);
            return View(order);
        }

        // GET: Order/Delete/5
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Orders == null)
            {
                return NotFound();
            }

            var order = await _context.Orders
                .Include(o => o.Member)
                .FirstOrDefaultAsync(m => m.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }
        [Authorize(Roles = "ADMIN")]
        [HttpGet]
        public async Task<IActionResult> Report([FromQuery] DateTime? startDate, [FromQuery] DateTime? endDate)
        {
            try
            {//https://localhost:7112/api/Order/Statistic?startDate=2002-01-01&endDate=2023-01-01
                if ((!startDate.HasValue || !endDate.HasValue)
                    && !(!startDate.HasValue && !endDate.HasValue)
                    )
                {
                    ViewData["SearchError"] = "Please input both start date and end date to generate a report!!";
                }

                ViewData["StartDate"] = startDate.Value.ToString("yyyy-MM-ddTHH:mm:ss");
                ViewData["EndDate"] = endDate.Value.ToString("yyyy-MM-ddTHH:mm:ss");
                HttpResponseMessage response;
                string fetchUrl = _url.Default +
                    "/Order/Statistic?startDate=" + startDate + "&endDate=" + endDate;

                response = await eStoreClientUtils.ApiRequest(
                    eStoreHttpMethod.GET,
                    fetchUrl);
                if (response.IsSuccessStatusCode)
                {
                    IEnumerable<SaleReportModel> orders =
                        await response.Content.ReadAsAsync<IEnumerable<SaleReportModel>>();
                    return View(orders);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("AccessDenied", "Login");
                }
                else
                {
                    throw new Exception(await response.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                ViewData["Orders"] = ex.Message;
                return View();
            }
        }
        // POST: Order/Delete/5
        [Authorize(Roles = "ADMIN")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Orders == null)
            {
                return Problem("Entity set 'FstoreDbContext.Orders'  is null.");
            }
            var order = await _context.Orders.FindAsync(id);
            if (order != null)
            {
                _context.Orders.Remove(order);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return (_context.Orders?.Any(e => e.OrderId == id)).GetValueOrDefault();
        }
        private void SetupPieChart(Pie pie,List<Table> data)
        {
            int amount = data.Count();
            for (int i = 0; i < amount; i++)
            {
                pie.data.labels[i] = data[i].Key;
                pie.data.datasets[0].backgroundColor[i] = GetRandomColour();
                pie.data.datasets[0].data[i] = data[i].Value;
            }
        }
        private static int xx = 0, yy = 0, zz = 0;
        private string GetRandomColour()
        {
            Random random = new Random();
            xx = (xx + DateTime.Now.Millisecond + DateTime.Now.Second + random.Next(24)) % 256;
            yy = (yy + DateTime.Now.Millisecond + DateTime.Now.Second + DateTime.Now.Hour) % 256;
            zz = (zz + DateTime.Now.Millisecond + DateTime.Now.Second + DateTime.Now.Minute) % 256;
            return "rgb(" + xx.ToString() + "," + yy.ToString() + "," + zz.ToString() + ")";
        }
        public async Task<IActionResult> RevenueStructure(int? option)
        {
            if (option == null)
                option = DateTime.Now.Year;

            ViewData["option"] = option;

            HttpResponseMessage response;
            string fetchUrl = _url.Default + "/Order/GetPieChart?year=" + option;

            try
            {
                response = await eStoreClientUtils.ApiRequest(
                    eStoreHttpMethod.GET,
                    fetchUrl);

                if (response.IsSuccessStatusCode)
                {
                    // Read the response content as JSON and deserialize it to your model
                    var jsonString = await response.Content.ReadAsStringAsync();
                    var orders = JsonConvert.DeserializeObject<List<Table>>(jsonString);

                    // Now, you can use the orders data as needed.

                    var data = orders;
                    int amount = data.Count();

                    Pie revenueStructure = new Pie(amount);
                    revenueStructure.options.plugins.title.text = "Cơ cấu doanh thu năm " + option;
                    SetupPieChart(revenueStructure, data);

                    return View(revenueStructure);
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    return RedirectToAction("AccessDenied", "Login");
                }
                else
                {
                    throw new Exception(await response.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions here, log or display an error message as needed.
                return View("Error"); // You can create an error view
            }
        }


    }
}
