﻿using AutoMapper;
using BusinessObject.Entities;
using Repository.Commons;
using Service.Model.Category;
using Service.Model.MemberModel;
using Service.Model.Order;
using Service.Model.Product;
using static Service.Model.Product.ProductCategoryModel;

namespace Service.AutoMapper
{
    public class MapperConfiguration : Profile
    {
        public MapperConfiguration()
        {
            CreateMap<Member, LoginModel>().ReverseMap();
            CreateMap<Member, CreateMemberModel>().ReverseMap();
            CreateMap<Member, UpdateMemberModel>().ReverseMap();
            CreateMap<Member, MemberViewModel>().ReverseMap();
            CreateMap<Product, ProductViewModel>().ReverseMap();
            CreateMap<CategoryProduct, Category>().ReverseMap();
            CreateMap<CategoryProductModel, Category>().ReverseMap();
            CreateMap<ProductCategoryModel, Product>().ReverseMap();
            CreateMap<CategoryProduct, Category>().ReverseMap();
            CreateMap<CreateProductModel, Product>().ReverseMap();
            CreateMap<UpdateProductModel, Product>().ReverseMap();
            CreateMap<CategoryViewModel, Category>().ReverseMap();
            CreateMap<OrderViewModel, Order>().ReverseMap();
            CreateMap<AddOrderDetail, OrderDetail>().ReverseMap();
            CreateMap<CreateOrderModel, Order>().ReverseMap();
            CreateMap<UpdateOrderModel, Order>().ReverseMap();
            CreateMap<OrderResponseModel, Order>().ReverseMap();
            CreateMap<OrderDetail, CreateOrderDetail>().ReverseMap();
            CreateMap<OrderDetail, AddOrderDetail>().ReverseMap();
            CreateMap<OrderRequestModel, Order>().ReverseMap();

            CreateMap(typeof(Pagination<>), typeof(Pagination<>));
        }
    }
}
