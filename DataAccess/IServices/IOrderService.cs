﻿using BusinessObject.Entities;
using Repository.Commons;
using Repository.Repositories;
using Service.Model.Order;
using Service.Model.SaleReportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.IServices
{
    public interface IOrderService
    {
        Task<Pagination<OrderViewModel>> GetOrderAsync(int pageIndex = 0, int pageSize = 10);
        Task<Pagination<OrderRequestModel>> GetOrderDetailByEachOrderAsync(int pageIndex = 0, int pageSize = 10);
        Task<Pagination<OrderViewModel>> GetOrdersAsync(int memberId, int pageIndex = 0, int pageSize = 10);
        Task<Pagination<OrderViewModel>> SearchOrderAsync(DateTime startDate, DateTime endDate, int pageIndex = 0, int pageSize = 10);
        Task<Order> GetOrderAsync(int orderId);
        Task<OrderResponseModel> AddOrderAsync(CreateOrderModel newOrder);
        Task<Order> UpdateOrderAsync(int orderId, UpdateOrderModel updatedOrder);
        Task<string> DeleteOrderAsync(int orderId);
        Task<int> GetNextId();
        Task<IEnumerable<SaleReportModel>> GetSalesReport(DateTime startDate, DateTime endDate);
        Task<decimal> CalculateSumOrderAllTime();
        Task<List<Table>> GetRevenueStructure(int year);
        Task<List<Table>> GetProductWithAmount(DateTime startDate, DateTime endDate);
    }
}
