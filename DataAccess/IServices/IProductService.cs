﻿using BusinessObject.Entities;
using Repository.Commons;
using Service.Model.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.IServices
{
    public interface IProductService
    {
        Task<Pagination<ProductViewModel>> GetProductsAsync(int pageIndex = 0, int pageSize = 10);
        Task<Product> GetProductAsync(int productId);
        Task<Pagination<ProductCategoryModel>> GetProductsAsync(int categoryId, int pageIndex = 0, int pageSize = 10);
        Task<IEnumerable<Product>> GetProductAsync(string productName);
        Task<Product> AddProductAsync(CreateProductModel request);
        Task<Product> UpdateProductAsync(int productId, UpdateProductModel updatedProduct);
        Task<string> DeleteProductAsync(int productId);
        Task<Pagination<ProductViewModel>> SearchProductAsync(decimal startPrice, decimal endPrice, int pageIndex = 0, int pageSize = 10);
        Task<IEnumerable<Product>> GetAllProduct();
    }
}
