﻿using BusinessObject.Entities;
using Repository.Commons;
using Service.Model.MemberModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.IServices
{
    public interface IMemberService
    {
        Member GetAdmin();
        Task<Member> LoginAsync(LoginModel loginModel);
        Task<Pagination<MemberViewModel>> GetMembersAsync(int pageIndex = 0, int pageSize = 10);
        Task<Member> GetMemberByIdAsync(int memberId);
        Task<Member> GetMemberByEmailAsync(string memberEmail);
        Task<Member> AddMemberAsync(CreateMemberModel request);
        Task<Member> UpdateMemberAsync(int memberId, UpdateMemberModel member);
        Task<string> DeleteMember(int memberId);
        Task<IEnumerable<Member>> GetMembersAsync();
    }
}
