﻿namespace Service.IServices
{
    public interface IClaimsServices
    {
        string? GetCurrentUser();
    }
}
