﻿using BusinessObject.Entities;
using Repository.Commons;
using Service.Model.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.IServices
{
    public interface ICategoryService
    {
        Task<Pagination<CategoryViewModel>> GetCategoriesAsync(int pageIndex = 0, int pageSize = 10);
        Task<Category> GetCategoryAsync(int categoryId);
        Task<Category> AddCategoryAsync(CreateCategoryModel newCategory);
        Task<Category> UpdateCategoryAsync(int categoryId, UpdateCategoryModel updatedCategory);
        Task<string> DeleteCategoryAsync(int categoryId);
        Task<IEnumerable<Category>> GetCategoryAsync();
    }
}
