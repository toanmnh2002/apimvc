﻿using AutoMapper;
using AutoMapper.Execution;
using Azure.Core;
using BusinessObject.Entities;
using BusinessObject.Enum;
using Microsoft.EntityFrameworkCore;
using Repository;
using Repository.Commons;
using Service.IServices;
using Service.Model.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Category> AddCategoryAsync(CreateCategoryModel request)
        {
            var checkCategory = await _unitOfWork.categoryRepo.GetEntityCondition(x => x.CategoryName == request.CategoryName);
            if (checkCategory is null)
            {
                throw new Exception("Category is not existed!");
            }
            var newcate = _mapper.Map<Category>(request);
            checkCategory.CategoryId = await GetNextId();
            await _unitOfWork.categoryRepo.CreateAsync(newcate);
            var isSucess = await _unitOfWork.SaveChangeAsync() > 0;
            return isSucess ? newcate : throw new Exception("Fail to create Category");
        }
        private async Task<int> GetNextId() => await _unitOfWork.productRepo.GetNextId();

        public async Task<string> DeleteCategoryAsync(int categoryId)
        {
            var category = await _unitOfWork.categoryRepo.GetEntityCondition(x => x.CategoryId == categoryId);
            if (category is null)
            {
                throw new Exception("Category is not existed!");
            }
            _unitOfWork.categoryRepo.DeleteAsync(category);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            return isSuccess ? "Success" : "Fail";
        }

        public async Task<Pagination<CategoryViewModel>> GetCategoriesAsync(int pageIndex = 0, int pageSize = 10)
        {
            var listCategories = await _unitOfWork.categoryRepo.FilterEntity(
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.CategoryId,
                sortDirection: SortDirection.Descending
                );
            var result = _mapper.Map<Pagination<CategoryViewModel>>(listCategories);
            return result;
        }
        public async Task<IEnumerable<Category>> GetCategoryAsync() => await _unitOfWork.categoryRepo.GetAllEntitiesGenericAsync();
        public async Task<Category> GetCategoryAsync(int categoryId) => await _unitOfWork.categoryRepo.GetEntityCondition(x => x.CategoryId == categoryId, include: x => x.Include(x => x.Products));
        public async Task<Category> UpdateCategoryAsync(int categoryId, UpdateCategoryModel updatedCategory)
        {
            var category = await _unitOfWork.categoryRepo.GetEntityCondition(x => x.CategoryId == categoryId);
            if (category is null)
            {
                throw new ApplicationException($"Category with the ID {categoryId} does not exist! Please check with the developer for more information");
            }
            _mapper.Map(updatedCategory, category);
            _unitOfWork.categoryRepo.UpdateAsync(category);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            var result = _mapper.Map<UpdateCategoryModel>(category);
            return isSuccess ? category : throw new Exception("Fail to update category");
        }
    }
}
