﻿using AutoMapper;
using AutoMapper.Execution;
using Azure.Core;
using BusinessObject.Entities;
using BusinessObject.Enum;
using Microsoft.EntityFrameworkCore;
using Repository;
using Repository.Commons;
using Service.IServices;
using Service.Model.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Product> AddProductAsync(CreateProductModel request)
        {
            var checkProduct = await _unitOfWork.productRepo.GetEntityCondition(x => x.CategoryId == request.CategoryId);
            if (checkProduct is null)
            {
                throw new Exception("Category is not existed!");
            }
            var newProduct = _mapper.Map<Product>(request);
            newProduct.ProductId = await GetNextId();
            await _unitOfWork.productRepo.CreateAsync(newProduct);
            var isSucess = await _unitOfWork.SaveChangeAsync() > 0;
            return isSucess ? newProduct : throw new Exception("Fail to create Member");
        }
        private async Task<int> GetNextId() => await _unitOfWork.productRepo.GetNextId();
        public async Task<string> DeleteProductAsync(int productId)
        {
            var product = await _unitOfWork.productRepo.GetEntityCondition(x => x.ProductId == productId);
            if (product is null)
            {
                throw new Exception("Product is not existed!");
            }
            _unitOfWork.productRepo.DeleteAsync(product);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            return isSuccess ? "Success" : "Fail";
        }

        public async Task<Product> GetProductAsync(int productId) => await _unitOfWork.productRepo.GetEntityCondition(x => x.ProductId == productId, include: x => x.Include(a => a.Category));
        public async Task<IEnumerable<Product>> GetAllProduct() => await _unitOfWork.productRepo.GetAllEntitiesGenericAsync(include:x=>x.Include(a=>a.Category));

        public async Task<IEnumerable<Product>> GetProductAsync(string productName) => await _unitOfWork.productRepo.GetAllEntitiesGenericAsync(x => x.ProductName.ToLower().Contains(productName.ToLower()), include: x => x.Include(x => x.Category));
        public async Task<Pagination<ProductViewModel>> GetProductsAsync(int pageIndex = 0, int pageSize = 10)
        {
            var listProduct = await _unitOfWork.productRepo.FilterEntity(
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.ProductId,
                include: x => x.Include(a => a.Category),
                sortDirection: SortDirection.Descending
                );
            var result = _mapper.Map<Pagination<ProductViewModel>>(listProduct);
            return result;
        }

        public async Task<Pagination<ProductCategoryModel>> GetProductsAsync(int categoryId, int pageIndex = 0, int pageSize = 10)
        {
            var listProduct = await _unitOfWork.productRepo.FilterEntity(x => x.CategoryId == categoryId,
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.ProductId,
                include: x => x.Include(a => a.Category),
                sortDirection: SortDirection.Descending
                );
            var result = _mapper.Map<Pagination<ProductCategoryModel>>(listProduct);
            return result;
        }
        public async Task<Pagination<ProductViewModel>> SearchProductAsync(decimal startPrice, decimal endPrice, int pageIndex = 0, int pageSize = 10)
        {
            if (startPrice > endPrice)
            {
                decimal temp = startPrice;
                startPrice = endPrice;
                endPrice = temp;
            }
            var list = await _unitOfWork.productRepo.FilterEntity(x => x.UnitPrice >= startPrice && x.UnitPrice <= endPrice, pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.ProductId,
                include: x => x.Include(a => a.Category),
                sortDirection: SortDirection.Descending);
            var result = _mapper.Map<Pagination<ProductViewModel>>(list);
            return result;
        }

        public async Task<Product> UpdateProductAsync(int productId, UpdateProductModel updatedProduct)
        {
            var product = await _unitOfWork.productRepo.GetEntityCondition(x => x.ProductId == productId);
            var category = await _unitOfWork.productRepo.GetEntityCondition(x => x.CategoryId == updatedProduct.CategoryId);
            if (product is null)
            {
                throw new Exception($"Product with the ID {product} does not exist! Please check with the developer for more information");
            }
            if (category is null)
            {
                throw new Exception($"Category with the ID {category} does not exist! Please check with the developer for more information");
            }
            _mapper.Map(updatedProduct, product);
            _unitOfWork.productRepo.UpdateAsync(product);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            var result = _mapper.Map<UpdateProductModel>(product);
            return isSuccess ? product : throw new Exception("Fail to update product");
        }
    }
}
