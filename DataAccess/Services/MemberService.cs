﻿using AutoMapper;
using BusinessObject.Entities;
using Repository.IServices;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using BusinessObject.Enum;
using Repository.Commons;
using Microsoft.EntityFrameworkCore;
using Service.Model.MemberModel;
using Repository;

namespace Service.Services
{
    public class MemberService : IMemberService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public MemberService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Member> AddMemberAsync(CreateMemberModel request)
        {
            var checkMember = await _unitOfWork.memberRepo.GetEntityCondition(x => x.Email == request.Email);
            if (checkMember is not null)
            {
                throw new Exception($"Member with email {request.Email} is existed!!");
            }
            var newMember = _mapper.Map<Member>(request);
            newMember.MemberId = await GetNextId();
            await _unitOfWork.memberRepo.CreateAsync(newMember);
            var isSucess = await _unitOfWork.SaveChangeAsync() > 0;
            return isSucess ? newMember : throw new Exception("Fail to create Member");
        }
        private async Task<int> GetNextId() => await _unitOfWork.memberRepo.GetNextId();
        public async Task<string> DeleteMember(int memberId)
        {
            var user = await _unitOfWork.memberRepo.GetEntityCondition(x => x.MemberId == memberId);
            if (user is null)
            {
                throw new Exception("Member is not existed!");
            }
            _unitOfWork.memberRepo.DeleteAsync(user);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            return isSuccess ? "Success" : "Fail";
        }

        public async Task<Member> GetMemberByEmailAsync(string memberEmail) => await _unitOfWork.memberRepo.GetEntityCondition(x => x.Email == memberEmail);

        public async Task<Member> GetMemberByIdAsync(int memberId) => await _unitOfWork.memberRepo.GetEntityCondition(x => x.MemberId == memberId);

        public async Task<Pagination<MemberViewModel>> GetMembersAsync(int pageIndex = 0, int pageSize = 10)
        {
            var listMember = await _unitOfWork.memberRepo.FilterEntity(
                pageIndex: pageIndex,
                pageSize: pageSize,
                sortColumn: x => x.MemberId,
                sortDirection: SortDirection.Descending
                );
            var result = _mapper.Map<Pagination<MemberViewModel>>(listMember);
            return result;
        }
        public async Task<IEnumerable<Member>> GetMembersAsync() => await _unitOfWork.memberRepo.GetAllEntitiesGenericAsync(include:x=>x.Include(x=>x.Orders));

        #region settings
        private static IConfigurationRoot GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true);
            return builder.Build();
        }
        private static string GetAdminContent => System.Text.Json.JsonSerializer.Serialize(
        new
        {
            Email = GetConfiguration()["Account:DefaultAccount:Email"],
            Password = GetConfiguration()["Account:DefaultAccount:Password"]
        }
    );
        public Member GetAdmin() => JsonConvert.DeserializeObject<Member>(GetAdminContent);
        #endregion
        public async Task<Member> LoginAsync(LoginModel loginModel)
        {
            //var user = await _unitOfWork.memberRepo.GetEntityCondition(x => x.Email == loginModel.Email || x.Password == loginModel.Password);
            //if (user is null)
            //{
            //    throw new Exception("Not found User!!!");
            //}
            IEnumerable<Member> members = await _unitOfWork.memberRepo.GetAllEntitiesGenericAsync(x => x.Email == loginModel.Email || x.Password == loginModel.Password);
            if (members is null)
            {
                throw new Exception("Not found User!!!");
            }
            members = members.Prepend(GetAdmin());
            return members.SingleOrDefault(member => member.Email.ToLower().Equals(loginModel.Email.ToLower())
                                    && member.Password.Equals(loginModel.Password));
        }

        public async Task<Member> UpdateMemberAsync(int memberId, UpdateMemberModel member)
        {
            var user = await _unitOfWork.memberRepo.GetEntityCondition(x => x.MemberId == memberId);
            if (user is null)
            {
                throw new ApplicationException($"Member with the ID {memberId} does not exist! Please check with the developer for more information");
            }
            if (!user.Email.Equals(member.Email))
            {
                throw new ApplicationException($"Email is not applicable to be updated!! Please try again...");
            }
            _mapper.Map(member, user);
            _unitOfWork.memberRepo.UpdateAsync(user);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            var result = _mapper.Map<UpdateMemberModel>(user);
            return isSuccess ? user : throw new Exception("Fail to update member");
        }
    }
}
