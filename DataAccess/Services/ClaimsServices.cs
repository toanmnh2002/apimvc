﻿using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace Service.IServices
{
    public class ClaimsServices : IClaimsServices
    {
        public ClaimsServices(IHttpContextAccessor httpContext)
        {
            var id = httpContext.HttpContext?.User?.Claims.FirstOrDefault(x => x.Type == "MemberId")?.Value;
            _currentUser = id;
        }
        private string? _currentUser { get; }

        public string? GetCurrentUser() => _currentUser;
    }
}
