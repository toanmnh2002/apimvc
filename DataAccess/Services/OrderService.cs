﻿using AutoMapper;
using AutoMapper.Execution;
using Azure.Core;
using BusinessObject.Entities;
using BusinessObject.Enum;
using eStoreAPI;
using Microsoft.EntityFrameworkCore;
using Repository;
using Repository.Commons;
using Repository.Repositories;
using Service.IServices;
using Service.Model.MemberModel;
using Service.Model.Order;
using Service.Model.SaleReportModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IClaimsServices _claimsServices;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper, IClaimsServices claimsServices)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _claimsServices = claimsServices;
        }
        public async Task<int> GetNextId() => await _unitOfWork.orderRepo.GetNextId();
        public async Task<OrderResponseModel> AddOrderAsync(CreateOrderModel newOrder)
        {

            var currentUser = _claimsServices.GetCurrentUser();
            if (currentUser is not null)
            {
                throw new Exception("User not logged in");
            }
            var order = _mapper.Map<Order>(newOrder);
            order.OrderId = await GetNextId();
            order.OrderDate = DateTime.Now;
            if (order.OrderDetails is not null && order.OrderDetails.Any())
            {
                foreach (var item in order.OrderDetails)
                {
                    var product = await _unitOfWork.productRepo.GetEntityCondition(x => x.ProductId == item.ProductId);
                    /// if product is not found for this ProductId
                    if (product is null)
                    {
                        throw new Exception($"Product not found for ProductId: {item.ProductId}");
                    }
                    if (product.UnitsInStock < item.Quantity)
                    {
                        throw new Exception("Order Quantity of '" + product.ProductName
                                            + "' is more than the units in stock! Please check again!!");
                    }
                    product.UnitsInStock -= item.Quantity;
                }
            }
            var result = _mapper.Map<OrderResponseModel>(order);
            await _unitOfWork.orderRepo.CreateAsync(order);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            return isSuccess ? result : throw new Exception("Fail to create Order");
        }

        public async Task<string> DeleteOrderAsync(int orderId)
        {
            var order = await _unitOfWork.orderRepo.GetEntityCondition(x => x.OrderId == orderId);
            if (order is null)
            {
                throw new Exception("Order is not existed!");
            }
            _unitOfWork.orderRepo.DeleteAsync(order);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            return isSuccess ? "Success" : "Fail";
        }

        public async Task<Pagination<OrderViewModel>> GetOrderAsync(int pageIndex = 0, int pageSize = 10)
        {
            var listOrder = await _unitOfWork.orderRepo.FilterEntity(
           pageIndex: pageIndex, include: x => x.Include(x => x.Member),
           pageSize: pageSize,
           sortColumn: x => x.OrderDate,
           sortDirection: SortDirection.Descending
           );
            var result = _mapper.Map<Pagination<OrderViewModel>>(listOrder);
            return result;
        }

        public async Task<Pagination<OrderViewModel>> GetOrdersAsync(int memberId, int pageIndex = 0, int pageSize = 10)
        {
            var listOrder = await _unitOfWork.orderRepo.FilterEntity(x => x.MemberId == memberId,
                  pageIndex: pageIndex, include: x => x.Include(x => x.Member).Include(x => x.OrderDetails),
                  pageSize: pageSize,
                  sortColumn: x => x.OrderDate,
                  sortDirection: SortDirection.Descending
                  );
            var result = _mapper.Map<Pagination<OrderViewModel>>(listOrder);
            return result;
        }

        public async Task<Pagination<OrderViewModel>> SearchOrderAsync(DateTime startDate, DateTime endDate, int pageIndex = 0, int pageSize = 10)
        {
            var listOrder = await _unitOfWork.orderRepo.FilterEntity(
                x => DateTime.Compare(x.OrderDate, startDate) >= 0 && DateTime.Compare(x.OrderDate, endDate) <= 0,
        pageIndex: pageIndex, include: x => x.Include(x => x.Member).Include(x => x.OrderDetails),
        pageSize: pageSize,
        sortColumn: x => x.OrderDate,
        sortDirection: SortDirection.Descending
        );
            var result = _mapper.Map<Pagination<OrderViewModel>>(listOrder);
            return result;
        }

        public async Task<Order> UpdateOrderAsync(int orderId, UpdateOrderModel updatedOrder)
        {
            var order = await _unitOfWork.orderRepo.GetEntityCondition(x => x.OrderId == orderId);
            if (order is null)
            {
                throw new Exception("Order is not existed!");
            }
            _mapper.Map(updatedOrder, order);
            _unitOfWork.orderRepo.UpdateAsync(order);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            var result = _mapper.Map<UpdateOrderModel>(order);
            return isSuccess ? order : throw new Exception("Fail to update order");
        }
        public async Task<decimal> CalculateTotalPriceAsync(IEnumerable<OrderDetail> orderDetails)
        {
            decimal totalPrice = 0;
            //item.UnitPrice * item.Quantity * (1 - (decimal)item.Discount / 100)
            foreach (var item in orderDetails)
            {
                var product = await _unitOfWork.productRepo.GetEntityCondition(x => x.ProductId == item.ProductId);
                /// if product is not found for this ProductId
                if (product is null)
                {
                    throw new Exception($"Product not found for ProductId: {item.ProductId}");
                }
                decimal itemPrice = product.UnitPrice * item.Quantity * (1 - (decimal)item.Discount / 100);
                totalPrice += itemPrice;
            }
            return totalPrice;
        }



        public async Task<IEnumerable<SaleReportModel>> GetSalesReport(DateTime startDate, DateTime endDate)
        {
            var orders = await _unitOfWork.orderRepo.FilterEntity(
               expression: o => o.OrderDate >= startDate && o.OrderDate <= endDate,
               include: o => o.Include(o => o.OrderDetails),
               pageIndex: 0,
               pageSize: int.MaxValue);

            var orderDetail = orders.Items.SelectMany(x => x.OrderDetails);
            var total = await CalculateTotalPriceAsync(orderDetail);


            var salesByDay = orders.Items.GroupBy(
                    o => new { Year = o.OrderDate.Year, Month = o.OrderDate.Month, Day = o.OrderDate.Day })
                .Select(g => new { Date = g.Key, TotalSales = total })
                .OrderBy(g => g.Date.Year)
                .ThenBy(g => g.Date.Month)
                .ThenBy(g => g.Date.Day)
                .ToList();

            var totalSales = salesByDay.Sum(s => s.TotalSales);

            var salesReport = new SaleReportModel
            {
                StartDate = startDate,
                EndDate = endDate,
                TotalSales = totalSales
            };
            return new List<SaleReportModel> { salesReport }; ;
        }
        public async Task<Order> GetOrderAsync(int orderId) => await _unitOfWork.orderRepo.GetEntityCondition(x => x.OrderId == orderId, include: x => x.Include(x => x.Member).Include(x => x.OrderDetails).ThenInclude(x => x.Product));
        public async Task<decimal> CalculateSumOrderAllTime()
        {
            var orders = await _unitOfWork.orderRepo.FilterEntity(
               include: o => o.Include(o => o.OrderDetails),
               pageIndex: 0,
               pageSize: int.MaxValue);
            var orderDetail = orders.Items.SelectMany(x => x.OrderDetails);
            var total = await CalculateTotalPriceAsync(orderDetail);
            return total;
        }
        public Task<List<Table>> GetRevenueStructure(int year) => _unitOfWork.orderDetailRepo.GetRevenueStructure(year);
        public Task<List<Table>> GetProductWithAmount(DateTime startDate, DateTime endDate) => _unitOfWork.orderDetailRepo.GetProductWithAmount(startDate, endDate);

        public async Task<Pagination<OrderRequestModel>> GetOrderDetailByEachOrderAsync(int pageIndex = 0, int pageSize = 10)
        {
            var listOrder = await _unitOfWork.orderRepo.FilterEntity(
     pageIndex: pageIndex, include: x => x.Include(x => x.OrderDetails),
     pageSize: pageSize,
     sortColumn: x => x.OrderDate,
     sortDirection: SortDirection.Descending
     );
            var result = _mapper.Map<Pagination<OrderRequestModel>>(listOrder);
            return result;
        }
    }

}
