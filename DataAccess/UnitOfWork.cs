﻿using BusinessObject.Entities;
using Repository;
using Repository.Interfaces;
using Repository.Repositories;
using Microsoft.EntityFrameworkCore;

namespace eStoreAPI
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FstoreDbContext _fstoreDbContext;

        public UnitOfWork(FstoreDbContext fstoreDbContext)
        {
            _fstoreDbContext = fstoreDbContext;
        }
        public IMemberRepo memberRepo => new MemberRepo(_fstoreDbContext);
        public IProductRepo productRepo => new ProductRepo(_fstoreDbContext);
        public ICategoryRepo categoryRepo => new CategoryRepo(_fstoreDbContext);
        public IOrderRepo orderRepo => new OrderRepo(_fstoreDbContext);
        public IOrderDetailRepo orderDetailRepo => new OrderDetailRepo(_fstoreDbContext);
        public Task<int> SaveChangeAsync()
        {
            return _fstoreDbContext.SaveChangesAsync();
        }
    }
}
