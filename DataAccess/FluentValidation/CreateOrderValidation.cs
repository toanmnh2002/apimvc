﻿using BusinessObject.Entities;
using FluentValidation;
using Service.Model.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.FluentValidation
{
    public class CreateOrderValidation : AbstractValidator<CreateOrderModel>
    {
        public CreateOrderValidation()
        {
            RuleFor(x => x.ShippedDate)
          .NotEmpty()
    .Must(shippedDate => shippedDate > DateTime.Now)
    .WithMessage("Shipped Date must be earlier than OrderDate");

            RuleFor(x => x.Freight)
    .NotNull().WithMessage("Freight is required.")
    .Must(BeValidNumericValue).WithMessage("Freight is not valid.");
        }

        private bool BeValidNumericValue(decimal? freight)
        {
            return freight.HasValue && freight >= 0;
        }
    }

}
