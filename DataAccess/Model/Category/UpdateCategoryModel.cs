﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Model.Category
{
    public class UpdateCategoryModel
    {
        public string CategoryName { get; set; }
    }
}
