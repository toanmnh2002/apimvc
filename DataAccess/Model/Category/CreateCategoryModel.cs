﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Model.Category
{
    public class CreateCategoryModel
    {
        public string CategoryName { get; set; }
    }
}
