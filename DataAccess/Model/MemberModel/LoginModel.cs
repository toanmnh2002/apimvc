﻿namespace Service.Model.MemberModel
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
