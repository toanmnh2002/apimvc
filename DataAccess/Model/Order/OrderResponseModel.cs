﻿using BusinessObject.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Model.Order
{
    public class OrderResponseModel
    {
        public int OrderId { get; set; }

        public int? MemberId { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime? RequiredDate { get; set; }

        public DateTime? ShippedDate { get; set; }

        public decimal? Freight { get; set; }
        public decimal TotalAmount => OrderDetails.Sum(x => x.PriceDiscount);
        public virtual ICollection<CreateOrderDetail> OrderDetails { get; set; }
    }

    public class CreateOrderDetail
    {
        public int ProductId { get; set; }

        public decimal UnitPrice { get; set; }

        public int Quantity { get; set; }

        public double Discount { get; set; }
        public decimal PriceDiscount => UnitPrice * Quantity * (1 - (decimal)Discount / 100);
        public decimal OrginalPrice => UnitPrice * Quantity;

    }
}
