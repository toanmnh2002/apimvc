﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Model
{
    public class ErrorValidationModel
    {
        public string FieldName { get; set; }
        public string ErrorMessage { get; set; }
    }
}
