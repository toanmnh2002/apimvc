﻿using AutoMapper;
using BusinessObject.Entities;
using Repository;
using Repository.IServices;
using eStoreAPI;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public static class DependencyInjections
    {
        public static IServiceCollection APIServices(this IServiceCollection services,IConfiguration configuration)
        {
            services.AddAutoMapper(typeof(DependencyInjections).Assembly);
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddDbContext<FstoreDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("DatabaseConnection")));
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(20);
                    options.LoginPath = "/";
                    options.LogoutPath = "/Logout";
                    options.AccessDeniedPath = "/Login/AccessDenied";
                });
            services.AddSession();
            services.AddAuthentication();
            services.AddAuthorization();
            services.AddControllersWithViews();
            services.AddHttpContextAccessor();
            services.AddSession(o =>
            {
                o.IdleTimeout = TimeSpan.FromMinutes(15);
            });
            return services;

        }
    }

}
