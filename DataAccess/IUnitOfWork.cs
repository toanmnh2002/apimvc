﻿using BusinessObject.Entities;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IUnitOfWork
    {
        public Task<int> SaveChangeAsync();
        public IMemberRepo memberRepo { get; }
        public IProductRepo productRepo { get; }
        public ICategoryRepo categoryRepo { get; }
        public IOrderRepo orderRepo { get; }
        public IOrderDetailRepo orderDetailRepo { get; }
    }
}
